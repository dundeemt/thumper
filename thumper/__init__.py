''' put Producer and Consumer in local thumper name space'''

# pylint: disable-msg=W0403

from rabbit import MQUri

from rabbit import Producer
from rabbit import Consumer
